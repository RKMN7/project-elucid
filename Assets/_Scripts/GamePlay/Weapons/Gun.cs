﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Weapon
{
    [SerializeField] protected Transform bulletSpawn;
    [SerializeField] protected int maxAmmo;
    public int MaxAmmo
    {
        get
        {
            return maxAmmo;
        }
    }
    public int currentAmmo { get; protected set; }
    

    //cache
    [SerializeField] protected AudioClip _shotSFX;

    protected override void Awake()
    {
        base.Awake();
        Initialize();
    }

    protected virtual void Initialize()
    {
		currentAmmo = maxAmmo;
	}

    public override void ResetWeapon()
    {
        currentAmmo = maxAmmo;
    }

    public override void OnPickup(Transform weaponSlot)
    {
        base.OnPickup(weaponSlot);
    }

    public override void OnReload()
    {

    }

    public override void Attack()
    {
        base.Attack();
        currentAmmo -= 1;
        if (currentAmmo < 0)
        {
            currentAmmo = 0;
        }
    }
}
