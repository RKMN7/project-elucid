﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Gun
{
	[SerializeField]
	private int bulletCount = 4;
	private int spreadAngle = 20;

	private float[] spread = new float[5];

	protected override void Initialize()
	{
		base.Initialize();
		weaponType = WeaponType.Shotgun;
		attackTime = 0.4f;
		reloadTime = 0.6f;
		for (int i = 0; i < bulletCount; ++i)
		{
			spread[i] = (i * spreadAngle / (bulletCount - 1))-spreadAngle/2;
		}
	}

    public override void ResetWeapon()
    {
        base.ResetWeapon();
    }

    public override void OnReload()
    {
        if (currentAmmo > 0)
        {
            _audioSource.PlayOneShot(_equipSFX);
        }
    }

    public override void Attack()
	{
		base.Attack();
		for (int i = 0; i < bulletCount; ++i)
		{
			Bullet bullet = GameManager.Instance.levelManager.bulletPool.Get();
            bullet.forWeaponType = weaponType;
			bullet.gameObject.SetActive(true);
			bullet.ShootBullet(bulletSpawn.position, bulletSpawn.rotation * Quaternion.Euler(0, spread[i], 0));
		}
        _audioSource.PlayOneShot(_shotSFX);
    }
}
