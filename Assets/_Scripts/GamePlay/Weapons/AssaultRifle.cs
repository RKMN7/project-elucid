﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultRifle : Gun
{
    [SerializeField]
    private int bulletCount = 3;

    protected override void Initialize()
    {
		base.Initialize();
		weaponType = WeaponType.AssaultRifle;
        attackTime = 0.6f;
		reloadTime = 0.75f;
    }

    public override void ResetWeapon()
    {
        base.ResetWeapon();
    }

    public override void Attack()
    {
        base.Attack();
        StartCoroutine(Fire());
    }

    IEnumerator Fire()
    {
        float bulletDelay = attackTime / bulletCount;
        float currentTime;
        for(int i = bulletCount; i> 0;--i)
        {
            currentTime = bulletDelay;
            Bullet bullet = GameManager.Instance.levelManager.bulletPool.Get();
            bullet.forWeaponType = weaponType;
            bullet.gameObject.SetActive(true);
            bullet.ShootBullet(bulletSpawn.position, bulletSpawn.rotation);
            _audioSource.PlayOneShot(_shotSFX);
            while (currentTime > 0)
            {
                yield return null;
                currentTime -= Time.deltaTime;
            }
        }
    }
}
