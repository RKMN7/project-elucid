﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sniper : Gun
{
    protected override void Initialize()
    {
        base.Initialize();
        weaponType = WeaponType.Sniper;
        attackTime = 0.2f;
        reloadTime = 1.0f;
    }

    public override void ResetWeapon()
    {
        base.ResetWeapon();
    }

    public override void OnReload()
    {
        if (currentAmmo > 0)
        {
            _audioSource.PlayOneShot(_equipSFX);
        }
    }

    public override void Attack()
    {
        base.Attack();
        Bullet bullet = GameManager.Instance.levelManager.sniperBulletPool.Get();
        bullet.forWeaponType = weaponType;
        bullet.gameObject.SetActive(true);
        bullet.ShootBullet(bulletSpawn.position, bulletSpawn.rotation);        
        _audioSource.PlayOneShot(_shotSFX);
    }
}
