﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IPoolable
{
	public GameObject sparksPrefab;
    [Tooltip("Defines what weapon uses this bullet")]
    public WeaponType forWeaponType;

	[SerializeField] int speed = 15;
    [SerializeField] int bounceCount = 1;

    //cache
    Rigidbody _rigidbody;
    Vector3 _velocity = new Vector3();
    int _bounces;

    public void Initialize()
	{
        _rigidbody = GetComponent<Rigidbody>();
	}

    public void OnGet()
    {
        _bounces = bounceCount;
    }

    public void ShootBullet(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
        _rigidbody.velocity = transform.forward * speed;
        _velocity = _rigidbody.velocity;
    }

    void OnCollisionEnter(Collision collision)
    {
        IDamageable damageableObj = collision.gameObject.GetComponent<IDamageable>();
        if (damageableObj != null)
        {
            damageableObj.TakeDamage(forWeaponType);
        }

        OnHit(collision.contacts[0].normal);
    }

	void OnTriggerEnter(Collider collider)
	{
        IDamageable damageableObj = collider.gameObject.GetComponent<IDamageable>();
        if(damageableObj != null)
        {
            damageableObj.TakeDamage(forWeaponType);
        }

        OnHit();
	}

    private void OnHit(Vector3? normal = null)
    {
        --_bounces;
        GameObject sparks = Instantiate(sparksPrefab, _rigidbody.position, Quaternion.identity);
        Destroy(sparks, 1);

        if (_bounces < 1)
        {
            GameManager.Instance.levelManager.bulletPool.Return(this);
        }
        else if(normal != null)
        {
            _rigidbody.velocity = Vector3.Reflect(_velocity, normal.Value);
            transform.rotation = Quaternion.FromToRotation(transform.forward, _rigidbody.velocity);
        }
    }

    public void ResetObject()
    {
        _rigidbody.velocity = Vector3.zero;        
        gameObject.SetActive(false);
    }
}
