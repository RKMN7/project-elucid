﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Handgun : Gun
{
    protected override void Initialize()
    {
        base.Initialize();
        weaponType = WeaponType.Handgun;
        attackTime = 0.2f;
		reloadTime = .5f;
    }

    public override void ResetWeapon()
    {
        base.ResetWeapon();
    }

    public override void Attack()
    {
        base.Attack();
        Bullet bullet = GameManager.Instance.levelManager.bulletPool.Get();
        bullet.forWeaponType = weaponType;
        bullet.gameObject.SetActive(true);
        bullet.ShootBullet(bulletSpawn.position, bulletSpawn.rotation);
        _audioSource.PlayOneShot(_shotSFX);
    }
}
