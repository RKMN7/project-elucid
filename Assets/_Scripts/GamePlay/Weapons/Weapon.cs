﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponState
{
    OnGround,
    Equipped,
    Projectile,
    Ragdoll
}

public enum WeaponEquippedState
{
    None,
    Ready,
    Attacking,
    Reloading,
    Empty
}

public enum WeaponType
{
    Handgun,
	Shotgun,
    AssaultRifle,
    Sniper,
    Throwable
}

[RequireComponent(typeof(Rigidbody), typeof(Collider), typeof(AudioSource))]
public class Weapon : MonoBehaviour
{
    public WeaponType weaponType { get; protected set; }
    public float projectileSpeed = 10;
    public Vector3 angularSpeed;       

    protected WeaponState weaponState;
    protected WeaponEquippedState weaponEquippedState;       

    public float attackTime { get; protected set; }
    public float reloadTime { get; protected set; }

    //cache
    protected Rigidbody _rigidbody;
    protected Collider _gunCollider;
    protected Coroutine _setStateCoroutine;
    protected AudioSource _audioSource;
    [SerializeField] protected AudioClip _equipSFX;

    protected virtual void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _gunCollider = GetComponent<Collider>();
        _audioSource = GetComponent<AudioSource>();
        _audioSource.outputAudioMixerGroup = GameManager.Instance.soundManager.sfxGroup;
    }

    public virtual void ResetWeapon()
    {

    }

	/// <summary>
	/// Throws the weapon in the given direction.
	/// </summary>
	/// <param name="direction"></param>
	public virtual void Throw(Vector3 direction)
    {
        transform.parent = null;
        transform.position += direction;
        transform.rotation = Quaternion.Euler(0, 0, 90);
        SetState(WeaponState.Projectile);
        _rigidbody.velocity = direction * projectileSpeed;
        _rigidbody.angularVelocity = angularSpeed;
    }

	/// <summary>
	/// Drops weapon to ground.
	/// </summary>
	public virtual void Drop()
	{
		transform.parent = null;
		transform.position += transform.up/2;
		SetState(WeaponState.Ragdoll);
	}

    /// <summary>
    /// This is called when weapon is picked up.
    /// </summary>
    public virtual void OnPickup(Transform weaponSlot)
    {
        transform.SetParent(weaponSlot);
        transform.localRotation = Quaternion.identity;
        transform.localPosition = Vector3.zero;
        SetState(WeaponState.Equipped);
        _audioSource.PlayOneShot(_equipSFX);
    }

    /// <summary>
    /// This is called when player enters weapon reload state.
    /// </summary>
    public virtual void OnReload()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
		if (weaponState == WeaponState.Projectile)
		{
			_rigidbody.velocity = Vector3.zero;
			SetState(WeaponState.Ragdoll);
			IDamageable damageableObj = collision.gameObject.GetComponent<IDamageable>();
			if (damageableObj != null)
			{
				damageableObj.Stun();
			}
		}
    }

    protected virtual void SetState(WeaponState newState)
    {
        ExitState(weaponState);
        weaponState = newState;
        EnterState(weaponState);
    }

    protected virtual void ExitState(WeaponState previousState)
    {
        switch(previousState)
        {
            case WeaponState.Equipped:
                break;
            case WeaponState.OnGround:
                break;
            case WeaponState.Projectile:
                break;
            case WeaponState.Ragdoll:
                _rigidbody.isKinematic = true;
                _rigidbody.useGravity = false;
                _rigidbody.velocity = Vector3.zero;
                break;
        }
    }

    protected virtual void EnterState(WeaponState nextState)
    {
        if (_setStateCoroutine != null)
        {
            StopCoroutine(_setStateCoroutine);
            _setStateCoroutine = null;
        }
        switch (nextState)
        {
            case WeaponState.Equipped:
                _rigidbody.isKinematic = true;
                _gunCollider.enabled = false;
                break;
            case WeaponState.OnGround:
                _rigidbody.isKinematic = true;
                _gunCollider.enabled = true;
                _gunCollider.isTrigger = true;
                break;
            case WeaponState.Projectile:
                _rigidbody.isKinematic = false;
                _rigidbody.useGravity = false;
                _gunCollider.enabled = true;
                _gunCollider.isTrigger = false;
                break;
            case WeaponState.Ragdoll:
				_gunCollider.enabled = true;
				_rigidbody.useGravity = true;
				_rigidbody.isKinematic = false;                
                _setStateCoroutine = StartCoroutine(SetStateAfterDelay(3, WeaponState.OnGround));
                break;
        }
    }

    IEnumerator SetStateAfterDelay(float delay, WeaponState nextState)
    {
        yield return new WaitForSeconds(delay);
        SetState(nextState);
        _setStateCoroutine = null;
    }

    public virtual void Attack()
    {

    }
}
