﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : Weapon
{
    protected override void Awake()
    {
        base.Awake();
        Initialize();
    }

    protected virtual void Initialize()
    {
        weaponType = WeaponType.Throwable;
    }

    public override void ResetWeapon()
    {

    }

    public override void OnPickup(Transform weaponSlot)
    {
        base.OnPickup(weaponSlot);
    }

    public override void OnReload()
    {

    }
}
