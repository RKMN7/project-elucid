﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCanvas : MonoBehaviour
{
    public static MainCanvas instance { get; private set; }
    public Canvas canvas { get; private set; }

    public PrecisionAim precisionAim;
    public VirtualJoystick leftJoystick;
    public VirtualJoystick rightJoystick;
    public Button attackButton;
    public Button throwPickupButton;
    public GameObject gameOverScreen;
    public Sprite pickupImage;
    public Sprite throwImage;
    public Text killCounter;


    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }
        instance = this;
        canvas = GetComponent<Canvas>();
    }

    void OnEnable()
    {
        GameManager.Instance.levelManager.OnPlayerDie += Death;
    }

    void OnDisable()
    {
        GameManager.Instance.levelManager.OnPlayerDie -= Death;
    }

    void Death()
    {
        gameOverScreen.SetActive(true);
    }

    public void LoadSceneButton(string sceneName)
    {
        GameManager.Instance.LoadScene(sceneName);
    }

    public void PickupImage(bool state)
    {
        if (state)
        {
            throwPickupButton.image.sprite = pickupImage;
        }
        else
        {
            throwPickupButton.image.sprite = throwImage;
        }
    }
}
