﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LaunchScene : MonoBehaviour
{
    [SerializeField] Slider sfxVolumeSlider;
    [SerializeField] Slider musicVolumeSlider;
    [SerializeField] Text statsText;

    private const string StatsFormat = "total play time (realtime): <color=#CF0000>{0:00}:{1:00}:{2:00}</color>\n" +
        "highest survival time (scaled time): <color=#CF0000>{3:00}:{4:00}:{5:00}</color>\n" +
        "highest kills: <color=#CF0000>{6}</color>\n" +
        "total kills: <color=#CF0000>{7}</color>\n" +
        "melee kills: <color=#CF0000>{8}</color>\n" +
        "handgun kills: <color=#CF0000>{9}</color>\n" +
        "shotgun kills: <color=#CF0000>{10}</color>\n" +
        "assault rifle kills: <color=#CF0000>{11}</color>\n" +
        "sniper kills: <color=#CF0000>{12}</color>";

    #region MonoBehaviour Callbacks

    private void Start()
    {
        sfxVolumeSlider.SetValueWithoutNotify(GameManager.Instance.userPrefs.sfxVolume);
        musicVolumeSlider.SetValueWithoutNotify(GameManager.Instance.userPrefs.musicVolume);
        InitializeStatsPage();
    }

    #endregion

    #region Public Methods

    public void OpenPage(GameObject page)
    {
        page.SetActive(true);
    }

    public void ClosePage(GameObject page)
    {
        page.SetActive(false);
    }

    public void Play()
    {
        GameManager.Instance.LoadScene("Gameplay");
    }

    public void MasterVolumeChanged(float level)
    {
        GameManager.Instance.soundManager.MasterVolume = level;
    }

    public void MusicVolumeChanged(float level)
    {
        GameManager.Instance.soundManager.MusicVolume = level;
    }

    public void SFXVolumeChanged(float level)
    {
        GameManager.Instance.soundManager.SFXVolume = level;
    }

    //public void MasterVolumeEditEnded(BaseEventData eventData)
    //{
    //    GameManager.Instance.soundManager.MasterVolume = masterVolumeSlider.value;
    //    GameManager.Instance.userPrefs.SaveUserPref();
    //}

    public void MusicVolumeEditEdned(BaseEventData eventData)
    {
        GameManager.Instance.soundManager.MusicVolume = musicVolumeSlider.value;
        GameManager.Instance.userPrefs.SaveUserPref();
    }

    public void SFXVolumeEditEnded(BaseEventData eventData)
    {
        GameManager.Instance.soundManager.SFXVolume = sfxVolumeSlider.value;
        GameManager.Instance.userPrefs.SaveUserPref();
    }

    public void ResetStats()
    {
        GameManager.Instance.userStats.ResetStats();
        InitializeStatsPage();
    }

    #endregion

    void InitializeStatsPage()
    {
        int totalSecs = (int)GameManager.Instance.userStats.TotalPlayTime;
        int totalHrs = (int)(totalSecs / 3600);
        totalSecs -= totalHrs * 3600;
        int totalMins = (int)(totalSecs / 60);
        totalSecs -= totalMins * 60;
        int highestSecs = (int)GameManager.Instance.userStats.HighestSurviveDuration;
        int highestHrs = (int)(highestSecs / 3600);
        highestSecs -= highestHrs * 3600;
        int highestMins = (int)(highestSecs / 60);
        highestSecs -= highestMins * 60;
        statsText.text = string.Format(StatsFormat, totalHrs, totalMins, totalSecs, highestHrs, highestMins, highestSecs, GameManager.Instance.userStats.HighestKills,
            GameManager.Instance.userStats.TotalKills, GameManager.Instance.userStats.MeleeKills, GameManager.Instance.userStats.HandgunKills,
            GameManager.Instance.userStats.ShotgunKills, GameManager.Instance.userStats.AssaultRifleKills, GameManager.Instance.userStats.SniperKills);
    }
}
