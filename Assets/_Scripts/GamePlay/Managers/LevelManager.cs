﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Delegates.Gamestate;

public class LevelManager
{
    public event PlayerDeath OnPlayerDie;

    public EnemyManager enemyManager;
    public MainCamera mainCam;
    public Player player;

    //TODO: create pools for each bullet type and only for bullet types.
    public ObjectPool<Bullet> bulletPool { get; private set; }
    public ObjectPool<Bullet> sniperBulletPool { get; private set; }

    public Transform poolsParent { get; private set; }

    //cache
    private PrecisionAim _precisionAim;
    private VirtualJoystick _leftJoystick;
    private VirtualJoystick _rightJoystick;

    public LevelManager()
    {
        enemyManager = new EnemyManager();
    }

    public void ConstructPools()
    {
        poolsParent = GameObject.Instantiate(Resources.Load<GameObject>("Gameplay/Prefabs/EmptyGameObject")).transform;
        poolsParent.name = "ObjectPools";
        GameObject prefab = Resources.Load<GameObject>("Gameplay/Prefabs/Bullets/Bullet");
        bulletPool = new ObjectPool<Bullet>(prefab, poolsParent, "BulletPool", 60);
        prefab = Resources.Load<GameObject>("Gameplay/Prefabs/Bullets/SniperBullet");
        sniperBulletPool = new ObjectPool<Bullet>(prefab, poolsParent, "SniperBulletPool", 10);
    }

    public void Initialize()
    {
        _precisionAim = MainCanvas.instance.precisionAim;
        _leftJoystick = MainCanvas.instance.leftJoystick;
        _rightJoystick = MainCanvas.instance.rightJoystick;
        ConstructPools();
        enemyManager.Initialize(poolsParent);
    }

    public void OnStartGameplay()
    {
        player.Initialize();
    }

    public void Update()
    {
        //Time Update
        BulletTime();

        //Player Update
        player.DoUpdate(_rightJoystick.virtualInput, _precisionAim.rotationDelta);

        enemyManager.Update(player.transform);
    }

    public void FixedUpdate()
    {
        player.DoPhysics(_leftJoystick.virtualInput);
    }

    public void LateUpdate()
    {
        //Camera Update
        mainCam.DoUpdate(player.transform.position, _rightJoystick.virtualInput);
    }

    void BulletTime()
    {
        if (_leftJoystick.virtualInput.magnitude <= 0.235)
        {
            Time.timeScale = 0.05f;
        }
        else
        {
            Time.timeScale = _leftJoystick.virtualInput.magnitude * _leftJoystick.virtualInput.magnitude / 1.1f;
        }

        Time.fixedDeltaTime = GameConstants.FIXED_DELTA_TIME * Time.deltaTime;
        GameManager.Instance.soundManager.SFXPitch = Time.timeScale;
    }

    public void ProcessPlayerDeath()
    {
        OnPlayerDie?.Invoke();
    }
}
