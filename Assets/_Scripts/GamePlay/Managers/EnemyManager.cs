﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager
{
    private List<Enemy> activeEnemies;
    private ObjectPool<Enemy> enemyPool;
    private List<GameObject> openSpawnPoints;
    private Queue<GameObject> usedSpawnPoints;

    private int availableEnemySlots;

    private float lastSpawnTime;

    //cache
    private GameObject _enemyPrefab;
    private WaitForSeconds spawnPtReleaseDelay = new WaitForSeconds(2);

    /// <summary>
    /// Creates new Enemy Manager and sets up the enemy pool.
    /// </summary>
    public EnemyManager()
    {        
        activeEnemies = new List<Enemy>();
    }

    /// <summary>
    /// Sets initial properties.
    /// </summary>
    public void Initialize(Transform poolParent)
    {
        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        openSpawnPoints = new List<GameObject>(spawnPoints);
        usedSpawnPoints = new Queue<GameObject>(spawnPoints.Length);
        _enemyPrefab = Resources.Load<GameObject>("Gameplay/Prefabs/Enemies/Enemy");
        enemyPool = new ObjectPool<Enemy>(_enemyPrefab, poolParent, "EnemyPool", 10);
        availableEnemySlots = 10;
    }

    /// <summary>
    /// Updates all the active enemies.
    /// </summary>
    public void Update(Transform player)
    {
        if(availableEnemySlots > 0 && Time.time - lastSpawnTime > 1)
        {
            SpawnEnemy(player);
        }
        for (int i = 0, n = activeEnemies.Count; i < n; ++i)
        {
            activeEnemies[i].UpdateState();
        }
    }

    /// <summary>
    /// Registers a new enemy. To be removed after the spawning is setup.
    /// </summary>
    /// <param name="enemy"></param>
    public void AddEnemy(Enemy enemy)
    {
        if (!activeEnemies.Contains(enemy))
        {
            activeEnemies.Add(enemy);
        }
    }

    /// <summary>
    /// Removes an enemy from active enemies list so it is no longer updated.
    /// </summary>
    /// <param name="enemy"></param>
    public void RemoveEnemy(Enemy enemy)
    {
        activeEnemies.Remove(enemy);
        enemyPool.Return(enemy);
        ++availableEnemySlots;
    }

    /// <summary>
    /// Spawns a new enemy.
    /// </summary>
    public void SpawnEnemy(Transform player, int count = 1)
    {
        int spawnPtCount = openSpawnPoints.Count;
        for (int i = count; i > 0; --i)
        {
            if (spawnPtCount > 0 && availableEnemySlots > 0)
            {
				float lastdistance=Mathf.Infinity;
				int index=0;
				float distance;
				for (int j=0; j<=spawnPtCount-1;++j)
				{
					distance = Vector3.Distance(openSpawnPoints[j].transform.position, player.position);
					if (distance<lastdistance)
					{
						lastdistance = distance;
						index = j;
					}
				}
                GameObject spawnPoint = openSpawnPoints[index];
                openSpawnPoints.RemoveAt(index);
                usedSpawnPoints.Enqueue(spawnPoint);
                Enemy enemy = enemyPool.Get();
                enemy.gameObject.SetActive(true);
                GameManager.Instance.StartCoroutine(SpawnAnimation(enemy, spawnPoint.transform));
                lastSpawnTime = Time.time;
                --spawnPtCount;
                --availableEnemySlots;
            }
        }
    }

    IEnumerator SpawnAnimation(Enemy enemy, Transform spawnPoint)
    {
        enemy.transform.rotation = Quaternion.LookRotation(spawnPoint.forward);
        float speed = 2;
        float distance = 3;
        float currentTime = 0;
        Vector3 start = spawnPoint.position - spawnPoint.forward * 1.5f;
        Vector3 end = spawnPoint.position + spawnPoint.forward  * (distance - 1.5f);
        start.y = end.y = 1.7f;
        while (currentTime < distance)
        {
            if(enemy.currentState == EnemyState.Dead)
            {
                yield break;
            }
            currentTime += Time.deltaTime * speed;
            enemy.transform.position = Vector3.Lerp(start, end, currentTime / distance);
            yield return null;
        }
        enemy.transform.position = end;
        enemy.OnSpawn();
        AddEnemy(enemy);
        GameManager.Instance.StartCoroutine(ReleaseUsedSpawnPoint());
    }

    IEnumerator ReleaseUsedSpawnPoint()
    {
        yield return spawnPtReleaseDelay;
        openSpawnPoints.Add(usedSpawnPoints.Dequeue());
    }
}
