﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Delegates.Input;
using UnityEngine.EventSystems;

public class InputManager
{
    public event TapAction OnTap;
    public event TouchStartAction OnTouchStart;
    public event TouchingAction OnTouching;
    public event TouchEndAction OnTouchEnd;

    public bool isEnabled { get; private set; }

    private int tapCount;
    private float touchTime;

    private Dictionary<int, TouchData> activeTouchData;
    private Stack<TouchData> touchDataPool;

    //cached
    private float _screenDensityFactor;
    private Vector2 _deltaPos;
    private TouchData _touchData;
    private PointerEventData _pointerEventData;
    private List<RaycastResult> _pointerUIDetectResults;

    public class TouchData
    {
        public Vector2 startPosition;
        public float startTime;
        public Vector2 position;
        public Vector2 deltaPosition;
        public float deltaTime;

        public TouchData()
        {
            startPosition = new Vector2();
            position = new Vector2();
            deltaPosition = new Vector2();
            startTime = Time.unscaledTime;
            deltaTime = 0;
        }
    }

    public InputManager()
    {
        isEnabled = true;
        _screenDensityFactor = Screen.height / 1080.0f;
        _deltaPos = new Vector2();
        _pointerEventData = new PointerEventData(EventSystem.current);
        _pointerUIDetectResults = new List<RaycastResult>(1);
        activeTouchData = new Dictionary<int, TouchData>();
        touchDataPool = new Stack<TouchData>();
        for (int i = 5; i > 0; --i)
        {
            touchDataPool.Push(new TouchData());
        }
    }

    public void Update()
    {
#if UNITY_EDITOR
        UpdatePointerData();
#else
        if (Input.touchCount > 0)
        {
            for (int i = 0, n = Input.touchCount; i < n; ++i)
            {
                UpdateTouchData(Input.touches[i]);
            }
        }
#endif
    }

    void UpdateTouchData(Touch touch)
    {
        switch (touch.phase)
        {
            case TouchPhase.Began:
                if (!ValidateTouch(touch.position))
                {
                    return;
                }
                _touchData.position = touch.position;
                _touchData.startPosition = touch.position;
                _touchData.startTime = Time.unscaledTime;
                if (!activeTouchData.ContainsKey(touch.fingerId))
                {
                    activeTouchData.Add(touch.fingerId, _touchData);
                }
                ProcessTouchStart(_touchData.startPosition, touch.fingerId);
                break;
            case TouchPhase.Stationary:
            case TouchPhase.Moved:
                if (!CacheTouchDataRefFromPool(touch.fingerId))
                {
                    return;
                }
                _touchData.position = touch.position;
                _touchData.deltaPosition = touch.deltaPosition;
                _touchData.deltaTime = touch.deltaTime;
                _deltaPos = _touchData.position - _touchData.startPosition;
                ProcessTouching(_touchData.position, _deltaPos, Time.unscaledTime - _touchData.startTime, touch.fingerId);
                break;
            case TouchPhase.Ended:
            case TouchPhase.Canceled:
                if (!CacheTouchDataRefFromPool(touch.fingerId))
                {
                    return;
                }
                _touchData.position = touch.position;
                _touchData.deltaPosition = touch.deltaPosition;
                _touchData.deltaTime = touch.deltaTime;
                activeTouchData.Remove(touch.fingerId);
                touchDataPool.Push(_touchData);
                _deltaPos = _touchData.position - _touchData.startPosition;
                ProcessTouchEnded(_touchData.position, _deltaPos, Time.unscaledTime - _touchData.startTime, touch.fingerId);
                break;
        }
    }

    void UpdatePointerData()
    {
        Vector2 position = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            if (!ValidateTouch(position))
            {
                return;
            }
            _touchData.startPosition = position;
            _touchData.startTime = Time.unscaledTime;
            _touchData.position = position;
            _touchData.deltaPosition = Vector2.zero;
            _touchData.deltaTime = 0;
            if (!activeTouchData.ContainsKey(0))
            {
                activeTouchData.Add(0, _touchData);
            }
            ProcessTouchStart(_touchData.position, 0);
        }
        else if (Input.GetMouseButton(0))
        {
            if (!CacheTouchDataRefFromPool(0))
            {
                return;
            }
            _touchData.position = position;
            _deltaPos = _touchData.position - _touchData.startPosition;
            ProcessTouching(_touchData.position, _deltaPos, Time.unscaledTime - _touchData.startTime, 0);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (!CacheTouchDataRefFromPool(0))
            {
                return;
            }
            activeTouchData.Remove(0);
            touchDataPool.Push(_touchData);
            _touchData.position = position;
            _deltaPos = _touchData.position - _touchData.startPosition;
            ProcessTouchEnded(_touchData.position, _deltaPos, Time.unscaledTime - _touchData.startTime, 0);
        }
    }

    /// <summary>
    /// Checks if the touch point contains any interactable UI elements.
    /// </summary>
    /// <param name="pointerPos"></param>
    /// <returns></returns>
    bool IsTouchedOverUI(Vector2 pointerPos)
    {
        _pointerUIDetectResults.Clear();
        _pointerEventData.position = pointerPos;
        EventSystem.current.RaycastAll(_pointerEventData, _pointerUIDetectResults);
        return _pointerUIDetectResults.Count > 0;
    }

    /// <summary>
    /// Check if touch is valid and assigns a touch data in active touches. Returns true if touch validation was successful.
    /// </summary>
    bool ValidateTouch(Vector2 touchPos)
    {
        if (IsTouchedOverUI(touchPos))
        {
            return false;
        }
        _touchData = touchDataPool.Pop();
        if (_touchData == null)
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Returns true if was able to fetch ref to touch data.
    /// </summary>
    /// <returns></returns>
    bool CacheTouchDataRefFromPool(int fingerId)
    {
        if (activeTouchData.ContainsKey(fingerId))
        {
            _touchData = activeTouchData[fingerId];
            return true;
        }
        return false;
    }

    void ProcessTouchStart(Vector2 position, int fingerId)
    {
        OnTouchStart?.Invoke(position, fingerId);
    }

    void ProcessTouching(Vector2 position, Vector2 deltaPosition, float touchDuration, int fingerId)
    {
        if (touchDuration >= GameConstants.TAP_TIME || deltaPosition.magnitude >= GameConstants.TAP_DELTA_POS / _screenDensityFactor)
        {
            if (--tapCount < 0)
            {
                tapCount = 0;
            }
            OnTouching?.Invoke(position, deltaPosition / _screenDensityFactor, deltaPosition.normalized, touchDuration, fingerId);
        }
        else
        {

        }
    }

    void ProcessTouchEnded(Vector2 position, Vector2 deltaPosition, float touchDuration, int fingerId)
    {
        if (touchDuration >= GameConstants.TAP_TIME || deltaPosition.magnitude >= GameConstants.TAP_DELTA_POS / _screenDensityFactor)
        {
            OnTouchEnd?.Invoke(position, deltaPosition / _screenDensityFactor, deltaPosition.normalized, touchDuration, fingerId);
        }
        else
        {
            OnTap?.Invoke(position, fingerId);
        }
    }
}
