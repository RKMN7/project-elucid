﻿public interface IDamageable
{
    void TakeDamage(WeaponType fromWeaponType);
	void Stun();
}
