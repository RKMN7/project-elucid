﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    [SerializeField]
    Vector3 offset;

    //cache
    float _distance;
	Vector3 _velocity = Vector3.one;	
	Vector3 _target = new Vector3();

    void Start()
	{
        GameManager.Instance.levelManager.mainCam = this;
	}

	public void DoUpdate(Vector3 playerPosition, Vector3 rightJoystickValue)
	{
		_target = playerPosition + offset + rightJoystickValue * 3;
        _distance = Vector3.Distance(transform.position, _target);
		transform.position = Vector3.SmoothDamp(transform.position, _target, ref _velocity, 0.1f, Mathf.Infinity, Time.unscaledDeltaTime);
	}
}
