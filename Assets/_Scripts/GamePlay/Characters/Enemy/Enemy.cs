﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyState
{
    Idle,
    Searching,
    Moving,
	Aiming,
    Attacking,
    Reloading,
	Stunned,
    Dead
}

[RequireComponent(typeof(NavMeshAgent), typeof(Rigidbody))]
public class Enemy : MonoBehaviour, IPoolable, IDamageable
{
    [Range(1, 20)]
    [SerializeField] float desiredRange = 10;
    [Range(1, 20)]
    [SerializeField] float maxVisionRange = 12;
    [Range(0, 360)]
    [SerializeField] float visionConeAngle = 40;
    GameObject player;

    public Weapon[] weaponPrefabs;

    public EnemyState currentState { get; private set; }
    public EnemyState previousState { get; private set; }

    public Weapon equippedWeapon;
    [SerializeField] Transform weaponSlot;
    public WeaponEquippedState weaponState { get; private set; }
    Weapon pickSelectedWeapon;

    //cache
    NavMeshAgent _navAgent;
    RaycastHit _hit;
	NavMeshHit _navMeshHit;
    Vector3 _vector3 = new Vector3();
    Vector3 _lastDestinationCheckPos = new Vector3();
	float WaitTime = 0;
	int check = 0;

	/// <summary>
	/// Initiliases the enemy for first use.
	/// </summary>
	public void Initialize()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        _navAgent = GetComponent<NavMeshAgent>();
		_navAgent.stoppingDistance = desiredRange;
    }

    /// <summary>
    /// Resets the condition to new.
    /// </summary>
    public void OnGet()
    {
        SetState(EnemyState.Idle);
        equippedWeapon = Instantiate(weaponPrefabs[Random.Range(0, weaponPrefabs.Length)]);
        EquipWeapon(equippedWeapon);
    }

    /// <summary>
    /// Gets this enemy ready for the gameplay after spawning.
    /// </summary>
    public void OnSpawn()
    {
        _navAgent.Warp(transform.position);
        SetState(EnemyState.Moving);
    }

    //private void OnEnable()
    //{
    //    GameManager.Instance.levelManager.enemyManager.AddEnemy(this);
    //}

    //private void OnDisable()
    //{
    //    GameManager.Instance.levelManager.enemyManager.RemoveEnemy(this);
    //}

    #region Enemy State Machine
    void SetState(EnemyState newState)
    {
        if (newState == currentState)
        {
            return;
        }
        previousState = currentState;
        currentState = newState;
        ExitState(previousState);
        EnterState(currentState);
    }

    private void EnterState(EnemyState state)
    {
        switch (state)
        {
            case EnemyState.Idle:
                break;
            case EnemyState.Searching:
				//Check LoS.
				if(CheckVision())
				{
					SetState(EnemyState.Attacking);
					break;
				}
				//Set number of checks and wait time for re-acquirement.
				WaitTime = 0.1f;
				check = 4;
				break;
            case EnemyState.Moving:
                //Set destination and start moving. Set waittime to recheck.
				_navAgent.SetDestination(player.transform.position);
                _lastDestinationCheckPos = player.transform.position;
                _navAgent.isStopped = false;
				WaitTime = .1f;
                break;
			case EnemyState.Aiming:
				//Start aiming for the player.
				WaitTime = 0;
				break;
            case EnemyState.Attacking:
				//Attack.
				transform.LookAt(player.transform);
				equippedWeapon.Attack();
				WaitTime = equippedWeapon.attackTime;
                break;
            case EnemyState.Reloading:
                //Set the reload cooldown.
                WaitTime = equippedWeapon.reloadTime;
				//WIP
				if(_navAgent.FindClosestEdge(out _navMeshHit))
				{
					if(_navMeshHit.distance >=2)
					{
						_navAgent.SetDestination(transform.position + new Vector3(_navMeshHit.position.x - transform.position.x, 0, _navMeshHit.position.z - transform.position.z).normalized);
						_navAgent.isStopped = false;
					}
					if (_navMeshHit.distance < 2)
					{
						_navAgent.SetDestination(transform.position + new Vector3(transform.position.x - _navMeshHit.position.x, 0, transform.position.z - _navMeshHit.position.z).normalized);
						_navAgent.isStopped = false;
					}
				}
                break;
			case EnemyState.Stunned:
				//Set stun time.
				WaitTime = 3;
				break;
            case EnemyState.Dead:
                if (equippedWeapon != null)
                {
                    DropWeapon();
                }
                break;
        }
    }

    public void UpdateState()
    {
        switch (currentState)
        {
            case EnemyState.Idle:
                break;
            case EnemyState.Searching:
				//Check if player in LoS, if yes then go to Attacking state, else go to Moving state.
				WaitTime -= Time.deltaTime;
				if (WaitTime <= 0)
				{
					if (CheckVision())
					{
						SetState(EnemyState.Aiming);
						break;
					}
					else
					{
						WaitTime = 0.1f;
						--check;
					}
				}
				if (check <= 0)
				{
					SetState(EnemyState.Moving);
				}
				break;
            case EnemyState.Moving:
				//Check if destination is arrived then go to Searching state.
				WaitTime -= Time.deltaTime;
                _vector3 = player.transform.position;
                if (Vector3.Distance(transform.position, _vector3) < desiredRange && !Physics.Linecast(transform.position, _vector3, LayerMasks.DefaultMask))
                {
					SetState(EnemyState.Aiming);
                    break;
                }
				if (Vector3.Distance(_vector3, _lastDestinationCheckPos) > .2f && WaitTime<=0)
				{
					_navAgent.SetDestination(_vector3);
					_lastDestinationCheckPos = _vector3;
                    WaitTime = .1f;
				}
				break;
			case EnemyState.Aiming:
				WaitTime += Time.deltaTime;
                if (WaitTime < 1 )
				{
                    if(Physics.Linecast(transform.position, player.transform.position, LayerMasks.DefaultMask))
                    {
                        SetState(EnemyState.Moving);
                        break;
                    }
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(player.transform.position - transform.position), WaitTime);
                    break;
				}
                SetState(EnemyState.Attacking);
                break;
			case EnemyState.Attacking:
				//Wait for aim, then shoot and set state to reloading.
				transform.LookAt(player.transform);
				WaitTime -= Time.deltaTime;
                if (WaitTime <= 0)
                {
                    SetState(EnemyState.Reloading);
                }
                break;
			case EnemyState.Reloading:
				transform.LookAt(player.transform);
				WaitTime -= Time.deltaTime;
                if (WaitTime <= 0)
                {
                    //Go to search state
                    SetState(EnemyState.Searching);
                }
                break;
			case EnemyState.Stunned:
				WaitTime -= Time.deltaTime;
				if(WaitTime<=0)
				{
					//Resume action
					SetState(EnemyState.Searching);
				}
				break;
			case EnemyState.Dead:
                break;
        }
    }

    private void ExitState(EnemyState state)
    {
        switch (state)
        {
            case EnemyState.Idle:
                break;
            case EnemyState.Searching:
                break;
            case EnemyState.Moving:
                _navAgent.isStopped = true;
                break;
			case EnemyState.Aiming:
				break;
			case EnemyState.Attacking:
                break;
            case EnemyState.Reloading:
				_navAgent.isStopped = true;
                break;
			case EnemyState.Stunned:
				break;
			case EnemyState.Dead:
                break;
        }
    }
    #endregion

    public void EquipWeapon(Weapon weapon)
    {
        equippedWeapon = weapon;
        equippedWeapon.OnPickup(weaponSlot);
    }

    public void DropWeapon()
    {
        equippedWeapon.ResetWeapon();
        equippedWeapon.Drop();
        equippedWeapon = null;
    }

    /// <summary>
    /// Checks if player is in this enemy's vision cone.
    /// </summary>
    /// <returns></returns>
    bool CheckVision()
    {
        _vector3 = player.transform.position - transform.position;
        _vector3.y = 0;
        if (Vector3.Distance(transform.position, player.transform.position) < maxVisionRange)
        {
            if (Vector3.Angle(_vector3, transform.forward) < visionConeAngle / 2.0f)
            {
                if (!Physics.Linecast(weaponSlot.position, player.transform.position, out _hit, LayerMasks.DefaultMask))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void TakeDamage(WeaponType fromWeaponType)
    {
        Die();
        GameManager.Instance.userStats.RegisterKill(fromWeaponType);
    }

	public void Stun()
	{
		EnterState(EnemyState.Stunned);
	}

    private void Die()
    {
        SetState(EnemyState.Dead);        
        //Return enemy to pool here.
        gameObject.SetActive(false);
        GameManager.Instance.levelManager.enemyManager.RemoveEnemy(this);
        //GameManager.Instance.StartCoroutine(RespawnEnemy(gameObject));
    }

    /// <summary>
    /// Called automatically when object is returned to pool.
    /// </summary>
    public void ResetObject()
    {
        //TODO: Reset the enemy properties to initial state and remove Destroy().
    }
}
