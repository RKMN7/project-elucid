﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour, IDamageable
{
	[SerializeField] float speed = 7;
	[SerializeField] float laserLength = 13;

    public Weapon equippedWeapon;
    [SerializeField] Transform weaponSlot;
    public WeaponEquippedState weaponState { get; private set; }
	float attackWaitTime = 0;
    Weapon pickSelectedWeapon;
    bool isDead;

	LineRenderer laser;

    //cache
    Gun _gun;
    Rigidbody _rigidbody;
    RaycastHit _hit;
    Vector3 _rotation;
    Collider[] _weaponsInRange = new Collider[10];
    Vector3 _vector3 = new Vector3();

	void Awake()
	{
		laser = GetComponent<LineRenderer>();
		_rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        GameManager.Instance.levelManager.player = this;        
    }

    public void Initialize()
    {
        isDead = false;
        EquipWeapon(equippedWeapon);
    }

    public void DoUpdate(Vector3 rightJoystickValue, float precisionRotationDelta)
	{
		laser.SetPosition(0, weaponSlot.transform.position);

        if (Physics.Raycast(transform.position, transform.forward, out _hit,laserLength))
        {
            laser.SetPosition(1, _hit.point);
        }
		else
		{
			laser.SetPosition(1, transform.position + transform.forward * laserLength);
		}

        UpdateWeaponState();

        if (rightJoystickValue != Vector3.zero)
        {
            transform.forward = Vector3.Lerp(transform.forward, rightJoystickValue, .5f);
        }
        else
        {
            transform.Rotate(Vector3.up, precisionRotationDelta);
        }
	}

	public void DoPhysics(Vector3 leftJoystickValue)
	{
		_rigidbody.MovePosition(transform.position + leftJoystickValue * speed * Time.deltaTime);
    }

    public void Attack()
    {
        equippedWeapon.Attack();
        SetWeaponState(WeaponEquippedState.Attacking);
    }

    public void EquipWeapon(Weapon weapon)
    {
        equippedWeapon = weapon;
        if (equippedWeapon.weaponType != WeaponType.Throwable)
        {
            _gun = (Gun)equippedWeapon;
        }
        equippedWeapon.OnPickup(weaponSlot);
        SetWeaponReady();
    }

    public void ThrowPickupWeapon()
    {
        if (weaponState == WeaponEquippedState.None)
        {
            EquipWeapon(pickSelectedWeapon);
        }
        else
        {
            equippedWeapon.Throw(transform.forward);
            SetWeaponState(WeaponEquippedState.None);
        }
    }

    #region Weapon state machine
    /// <summary>
    /// Should be called each frame when equipped.
    /// </summary>
    private void UpdateWeaponState()
    {
        switch (weaponState)
        {
            case WeaponEquippedState.None:
                CheckForWeapon();
				MainCanvas.instance.throwPickupButton.interactable = (pickSelectedWeapon != null);
                return;
            case WeaponEquippedState.Ready:
                break;
            case WeaponEquippedState.Attacking:
                attackWaitTime -= Time.deltaTime;
                if (attackWaitTime <= 0)
                {
					SetWeaponState(WeaponEquippedState.Reloading);
                }
                break;
            case WeaponEquippedState.Reloading:
				attackWaitTime -= Time.deltaTime;
				if (attackWaitTime <= 0)
				{
					SetWeaponReady();
				}
				break;
            case WeaponEquippedState.Empty:
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Checks weapon type and available ammo to set weapon state to ready or empty.
    /// </summary>
    private void SetWeaponReady()
    {
        if (_gun != null)
        {
            if (_gun.currentAmmo > 0)
            {
                SetWeaponState(WeaponEquippedState.Ready);
            }
            else
            {
                SetWeaponState(WeaponEquippedState.Empty);
            }
        }
        else
        {
            SetWeaponState(WeaponEquippedState.Ready);
        }
    }

    private void SetWeaponState(WeaponEquippedState newState)
    {
        if(newState == weaponState)
        {
            return;
        }
        OnExitWeaponState(weaponState);
        weaponState = newState;
        OnEnterWeaponState(weaponState);
    }

    private void OnEnterWeaponState(WeaponEquippedState state)
    {
        switch (state)
        {
            case WeaponEquippedState.None:
				MainCanvas.instance.PickupImage(true);
                MainCanvas.instance.attackButton.interactable = false;
                equippedWeapon = null;
                _gun = null;
                laser.enabled = false;
                break;
            case WeaponEquippedState.Ready:
                MainCanvas.instance.attackButton.interactable = (_gun != null);
                break;
            case WeaponEquippedState.Attacking:
                attackWaitTime = equippedWeapon.attackTime;
                MainCanvas.instance.throwPickupButton.interactable = false;
                break;
            case WeaponEquippedState.Reloading:
                if (_gun != null)
                {
                    equippedWeapon.OnReload();
                }
                attackWaitTime = equippedWeapon.reloadTime;
                break;
            case WeaponEquippedState.Empty:
                break;
            default:
                break;
        }
    }

    private void OnExitWeaponState(WeaponEquippedState state)
    {
        switch (state)
        {
            case WeaponEquippedState.None:
				MainCanvas.instance.PickupImage(false);
                pickSelectedWeapon = null;
				laser.enabled = true;
                break;
            case WeaponEquippedState.Ready:
                MainCanvas.instance.attackButton.interactable = false;
                break;
            case WeaponEquippedState.Attacking:
                attackWaitTime = 0;
				MainCanvas.instance.throwPickupButton.interactable = true;
                break;
            case WeaponEquippedState.Reloading:
                break;
            case WeaponEquippedState.Empty:
                break;
            default:
                break;
        }
    }

    void CheckForWeapon()
    {
        float distance;
        float minDistance = Mathf.Infinity;
        int weaponsDetected = Physics.OverlapCapsuleNonAlloc(transform.position - Vector3.up * 1.2f, transform.position + Vector3.up * 1.2f, 2,_weaponsInRange, LayerMasks.WeaponMask);
        if (weaponsDetected > 0)
        {
            if (weaponsDetected > 10)
            {
                weaponsDetected = 10;
            }
            for (int i = 0; i < weaponsDetected; ++i)
            {
                _vector3 = _weaponsInRange[i].transform.position;
                _vector3.y = transform.position.y;
                distance = Vector3.Distance(transform.position, _vector3);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    pickSelectedWeapon = _weaponsInRange[i].GetComponent<Weapon>();
                }
            }
        }
        else
        {
            pickSelectedWeapon = null;
        }
    }

    #endregion

    public void TakeDamage(WeaponType fromWeaponType)
    {
        if (isDead)
        {
            return;
        }
        Die();
        GameManager.Instance.levelManager.ProcessPlayerDeath();
    }

	public void Stun(){}

    void Die()
	{
        isDead = true;
        GameManager.Instance.userStats.RegisterDeath();
		Destroy(this.gameObject.GetComponent<MeshRenderer>());
	}
}
