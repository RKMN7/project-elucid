﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrecisionAim : MonoBehaviour
{
    [Range(1f, 10)]
    public float sensitivity;
    public float rotationDelta { get; private set; }

    RectTransform thisRT;
    int currentFinger = -1;

    //cache
    Vector2 _previousPosition = new Vector2();
    
    void Awake()
    {
        thisRT = GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        GameManager.Instance.inputController.OnTouchStart += OnTouchStart;
        GameManager.Instance.inputController.OnTouching += OnTouching;
        GameManager.Instance.inputController.OnTouchEnd += OnTouchEnd;
    }

    private void OnDisable()
    {
        GameManager.Instance.inputController.OnTouchStart -= OnTouchStart;
        GameManager.Instance.inputController.OnTouching -= OnTouching;
        GameManager.Instance.inputController.OnTouchEnd -= OnTouchEnd;
    }

    private void OnTouchStart(Vector2 position, int fingerId)
    {
        if (currentFinger == -1)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(thisRT, position))
            {
                currentFinger = fingerId;
                _previousPosition = position;
            }
        }
    }

    private void OnTouching(Vector2 position, Vector2 deltaPosition, Vector2 direction, float touchTime, int fingerId)
    {
        if (currentFinger == fingerId)
        {
            rotationDelta = (position.x - _previousPosition.x) * sensitivity * Time.unscaledDeltaTime;
            _previousPosition = position;
        }
    }

    private void OnTouchEnd(Vector2 position, Vector2 deltaPosition, Vector2 direction, float touchTime, int fingerId)
    {
        if (currentFinger == fingerId)
        {
            currentFinger = -1;
            rotationDelta = 0;
            _previousPosition = Vector2.zero;
        }
    }

    public void CancelAim()
    {

    }
}
