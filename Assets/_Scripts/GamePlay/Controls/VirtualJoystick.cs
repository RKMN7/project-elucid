﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class VirtualJoystick : MonoBehaviour
{
    public Vector3 virtualInput { get; private set; }
    Image joystickBackground;
    Image joystickTip;
    RectTransform thisRT;
    int currentFinger = -1;
    bool joystickOnScreen;

	//cache
	Vector2 _sizeDelta;

    private void Awake()
    {
        joystickBackground = transform.GetChild(0).GetComponent<Image>();
        joystickTip = joystickBackground.transform.GetChild(0).GetComponent<Image>();
        joystickBackground.gameObject.SetActive(false);
        virtualInput = Vector3.zero;
        thisRT = GetComponent<RectTransform>();
		_sizeDelta = joystickBackground.rectTransform.sizeDelta;
	}

    private void OnEnable()
    {
        GameManager.Instance.inputController.OnTouchStart += OnTouchStart;
        GameManager.Instance.inputController.OnTouching += OnTouching;
        GameManager.Instance.inputController.OnTouchEnd += OnTouchEnd;
        GameManager.Instance.inputController.OnTap += OnTap;
    }

    private void OnDisable()
    {
        GameManager.Instance.inputController.OnTouchStart -= OnTouchStart;
        GameManager.Instance.inputController.OnTouching -= OnTouching;
        GameManager.Instance.inputController.OnTouchEnd -= OnTouchEnd;
        GameManager.Instance.inputController.OnTap -= OnTap;
    }

    private void OnTouchStart(Vector2 position, int fingerId)
    {
        if(currentFinger == -1)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(thisRT, position))
            {
                currentFinger = fingerId;
                Vector2 loc;                
                RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRT, position, null, out loc);
                joystickBackground.rectTransform.anchoredPosition = loc;
            }
        }
    }

    private void OnTouching(Vector2 position, Vector2 deltaPosition, Vector2 direction, float touchTime, int fingerId)
    {
        if (currentFinger == fingerId)
        {
            if(!joystickOnScreen)
            {
                joystickBackground.gameObject.SetActive(true);
                joystickOnScreen = true;
            }
            deltaPosition.x /= (_sizeDelta.x / 2.0f);
            deltaPosition.y /= (_sizeDelta.y / 2.0f);
            if (deltaPosition.magnitude > 1)
            {
                deltaPosition.Normalize();
            }
            joystickTip.rectTransform.anchoredPosition = new Vector3(deltaPosition.x * (_sizeDelta.x / 3), deltaPosition.y * (_sizeDelta.y) / 3);
            virtualInput = new Vector3(deltaPosition.x, 0, deltaPosition.y);
        }
    }

    private void OnTouchEnd(Vector2 position, Vector2 deltaPosition, Vector2 direction, float touchTime, int fingerId)
    {
        if (currentFinger == fingerId)
        {
            DismissJoystick();
        }
    }

    private void OnTap(Vector2 position, int fingerId)
    {
        if(currentFinger == fingerId)
        {
            DismissJoystick();
        }
    }

    void DismissJoystick()
    {
        virtualInput = Vector3.zero;
        joystickTip.rectTransform.anchoredPosition = Vector3.zero;
        joystickBackground.gameObject.SetActive(false);
        joystickOnScreen = false;
        currentFinger = -1;
    }

    public Vector3 GetAxis()
    {
        return virtualInput;
    }
}
