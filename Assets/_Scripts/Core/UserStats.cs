﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserStats
{
    #region Properties

    public float TotalPlayTime
    {
        get
        {
            return totalPlayTime;
        }
    }

    public float HighestSurviveDuration
    {
        get
        {
            return highestSurviveDuration;
        }
    }

    public int HighestKills
    {
        get
        {
            return highestKills;
        }
    }

    public float TotalKills
    {
        get
        {
            return meleeKills + handgunKills + shotgunKills + assaultRifleKills + sniperKills;
        }
    }

    public float MeleeKills
    {
        get
        {
            return meleeKills;
        }
    }

    public float HandgunKills
    {
        get
        {
            return handgunKills;
        }
    }

    public float ShotgunKills
    {
        get
        {
            return shotgunKills;
        }
    }

    public float AssaultRifleKills
    {
        get
        {
            return assaultRifleKills;
        }
    }

    public float SniperKills
    {
        get
        {
            return sniperKills;
        }
    }

    public float Deaths
    {
        get
        {
            return deaths;
        }
    }

    #endregion

    #region Fields

    [SerializeField] private float totalPlayTime;
    [SerializeField] private float highestSurviveDuration;
    [SerializeField] private int highestKills;
    [SerializeField] private int meleeKills;
    [SerializeField] private int handgunKills;
    [SerializeField] private int shotgunKills;
    [SerializeField] private int assaultRifleKills;
    [SerializeField] private int sniperKills;
    [SerializeField] private int deaths;

    float currentSurviveDuration;
    int currentKillCount;

    #endregion

    public UserStats()
    {
        SetDefaultValues();
    }

    private void SetDefaultValues()
    {
        totalPlayTime = 0f;
        highestSurviveDuration = 0f;
        highestKills = 0;
        meleeKills = 0;
        handgunKills = 0;
        shotgunKills = 0;
        assaultRifleKills = 0;
        sniperKills = 0;
        deaths = 0;
    }

    #region Public Methods
    public void SaveUserStats()
    {
        FileManager.WriteToFile(Application.persistentDataPath + Constants.UserStatsPath, this);
    }

    public void LoadUserStats()
    {
        UserStats savedStats = FileManager.ReadFromFile<UserStats>(Application.persistentDataPath + Constants.UserStatsPath, true);
        this.CopyFrom(savedStats);
    }

    public void ResetStats()
    {
        SetDefaultValues();
        SaveUserStats();
    }

    /// <summary>
    /// Initializes values which have scope only in one gameplay session.
    /// </summary>
    public void OnEnterGameplayMode()
    {
        currentSurviveDuration = 0;
        currentKillCount = 0;
		MainCanvas.instance.killCounter.text = currentKillCount.ToString();
    }

    /// <summary>
    /// Should be called once per frame in gameplay mode.
    /// </summary>
    public void Update()
    {
        currentSurviveDuration += Time.deltaTime;
        totalPlayTime += Time.unscaledDeltaTime;
    }

    /// <summary>
    /// Registers a kill to the kill count of the given weapon.
    /// </summary>
    /// <param name="weaponType"></param>
    public void RegisterKill(WeaponType weaponType)
    {
        currentKillCount++;
		MainCanvas.instance.killCounter.text = currentKillCount.ToString();
		switch (weaponType)
        {
            case WeaponType.Handgun:
                handgunKills++;
                break;
            case WeaponType.Shotgun:
                shotgunKills++;
                break;
            case WeaponType.AssaultRifle:
                assaultRifleKills++;
                break;
            case WeaponType.Sniper:
                sniperKills++;
                break;
            default:
                meleeKills++;
                break;
        }
    }

    /// <summary>
    /// Updates the player death count and saves user stats.
    /// </summary>
    public void RegisterDeath()
    {
        deaths++;
        if (currentSurviveDuration > highestSurviveDuration)
        {
            highestSurviveDuration = currentSurviveDuration;
        }
        currentSurviveDuration = 0;
        if(currentKillCount > highestKills)
        {
            highestKills = currentKillCount;
        }
        currentKillCount = 0;
        SaveUserStats();
    }

    #endregion 

    void CopyFrom(UserStats stats)
    {
        this.totalPlayTime = stats.totalPlayTime;
        this.highestSurviveDuration = stats.highestSurviveDuration;
        this.meleeKills = stats.meleeKills;
        this.handgunKills = stats.handgunKills;
        this.shotgunKills = stats.shotgunKills;
        this.assaultRifleKills = stats.assaultRifleKills;
        this.sniperKills = stats.sniperKills;
        this.deaths = stats.deaths;
    }
}
