﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class SoundManager
{
    [SerializeField] private AudioMixer mixer;
    public AudioMixerGroup masterGroup;
    public AudioMixerGroup musicGroup;
    public AudioMixerGroup sfxGroup;

    #region Volume Properties
    public float MasterVolume
    {
        get
        {
            //float result;
            //mixer.GetFloat("MasterVolume", out result);
            //return Mathf.Pow(10, result / 20);
            return GameManager.Instance.userPrefs.masterVolume;
        }
        set
        {
            float level = Mathf.Clamp(value, 0.0001f, 1.0f);
            mixer.SetFloat(_masterVolume, Mathf.Log10(level) * 20);
            GameManager.Instance.userPrefs.masterVolume = level;
        }
    }

    public float MusicVolume
    {
        get
        {
            //float result;
            //mixer.GetFloat("MusicVolume", out result);
            //return Mathf.Pow(10, result / 20);
            return GameManager.Instance.userPrefs.musicVolume;
        }
        set
        {
            float level = Mathf.Clamp(value, 0.0001f, 1.0f);
            mixer.SetFloat(_musicVolume, Mathf.Log10(level) * 20);
            GameManager.Instance.userPrefs.musicVolume = level;
        }
    }

    public float SFXVolume
    {
        get
        {
            //float result;
            //mixer.GetFloat("SFXVolume", out result);
            //return Mathf.Pow(10, result / 20);
            return GameManager.Instance.userPrefs.sfxVolume;

        }
        set
        {
            float level = Mathf.Clamp(value, 0.0001f, 1.0f);
            mixer.SetFloat(_sfxVolume, Mathf.Log10(level) * 20);
            GameManager.Instance.userPrefs.sfxVolume = level;
        }
    }
    #endregion

    #region Pitch Properties
    public float MasterPitch
    {
        get
        {
            float result;
            mixer.GetFloat(_masterPitch, out result);
            return result;

        }
        set
        {
            mixer.SetFloat(_masterPitch, value);
        }
    }

    public float MusicPitch
    {
        get
        {
            float result;
            mixer.GetFloat(_musicPitch, out result);
            return result;

        }
        set
        {
            mixer.SetFloat(_musicPitch, value);
        }
    }

    public float SFXPitch
    {
        get
        {
            float result;
            mixer.GetFloat(_sfxPitch, out result);
            return result;

        }
        set
        {
            mixer.SetFloat(_sfxPitch, Mathf.Clamp(value, 0.35f, 1));
        }
    }
    #endregion

    #region Constants
    private const string _masterVolume = "MasterVolume";
    private const string _musicVolume = "MusicVolume";
    private const string _sfxVolume = "SFXVolume";
    private const string _masterPitch = "MasterPitch";
    private const string _musicPitch = "MusicPitch";
    private const string _sfxPitch = "SFXPitch";
    #endregion

    #region Public Methods
    public void Initialize()
    {
        mixer.SetFloat(_masterVolume, Mathf.Log10(GameManager.Instance.userPrefs.masterVolume) * 20);
        mixer.SetFloat(_musicVolume, Mathf.Log10(GameManager.Instance.userPrefs.musicVolume) * 20);
        mixer.SetFloat(_sfxVolume, Mathf.Log10(GameManager.Instance.userPrefs.sfxVolume) * 20);
    }
    #endregion
}
