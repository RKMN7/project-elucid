﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Delegates.Gamestate;
using System;

public enum GameState
{
    Waiting,
    Gameplay,
    Paused
}

public class GameManager : MonoBehaviour
{
    public SoundManager soundManager;
    public UserPrefs userPrefs { get; private set; }
    public UserStats userStats { get; private set; }

    public InputManager inputController;
    public static GameManager Instance { get; private set; }

    public LevelManager levelManager;

    public GameState gameState { get; private set; }
    public SceneType sceneType { get; private set; }

    private SceneLoader sceneLoader;
    private Scene loadingScene;
    private string currentScene;

    //Use this to get self references and constructions.
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this);

        //Initialize static classes
        LayerMasks.Initialize();

        //Load user data
        userPrefs = FileManager.ReadFromFile<UserPrefs>(Application.persistentDataPath + Constants.UserPrefsPath, true);
        userStats = FileManager.ReadFromFile<UserStats>(Application.persistentDataPath + Constants.UserStatsPath, true);

        //Construct managers
        inputController = new InputManager();
        levelManager = new LevelManager();
        sceneLoader = new SceneLoader();
        currentScene = SceneManager.GetActiveScene().name;
        SceneManager.sceneLoaded += OnSceneLoaded;
        //TODO: remove this when launch scene is setup.
        EnterScene(SceneManager.GetActiveScene().name);
        //SetGameState(GameState.Gameplay);
    }

    //Use this to initialize members
    private void Start()
    {
        soundManager.Initialize();
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        switch (scene.name)
        {
            case "Loading":
                break;
            case "Gameplay":                
                levelManager.Initialize();
                SceneManager.MoveGameObjectToScene(levelManager.poolsParent.gameObject, SceneManager.GetSceneByName("Gameplay"));
                break;
            default:
                break;
        }
    }

    public void OnLoadingFinished(string sceneLoaded)
    {
        switch (sceneLoaded)
        {
            case "Gameplay":
                levelManager.OnStartGameplay();
                userStats.OnEnterGameplayMode();
                break;
            default:
                break;
        }
    }

    void Update()
    {
        if (!sceneLoader.isLoading)
        {
            UpdateGameState();
        }
    }

    private void FixedUpdate()
    {
        FixedUpdateGameState();
    }

    private void LateUpdate()
    {
        if (!sceneLoader.isLoading)
        {
            LateUpdateGameState();
        }
    }

    public void LoadScene(string sceneName)
    {
        ExitScene(currentScene);
        SceneManager.LoadScene("Loading"); //Intermediate scene to show loading.
        StartCoroutine(sceneLoader.LoadScene(sceneName)); //Actual scene to be leaded.
        currentScene = sceneName;
        EnterScene(currentScene);
    }

    /// <summary>
    /// Resets prvious scene type properties.
    /// </summary>
    /// <param name="sceneName"></param>
    public void ExitScene(string sceneName)
    {
        StopAllCoroutines();
        switch (sceneName)
        {
            case "Loading":
                break;
            case "Gameplay":
                MainCanvas.instance.gameObject.SetActive(false);
                levelManager = null;
                Time.timeScale = 1;
                Time.fixedDeltaTime = GameConstants.FIXED_DELTA_TIME;
                GameManager.Instance.soundManager.SFXPitch = Time.timeScale;
                break;
            default:
                break;
        }
        GC.Collect();
    }

    /// <summary>
    /// Sets current scene type and sets game manager properties for next scene.
    /// </summary>
    /// <param name="sceneName"></param>
    public void EnterScene(string sceneName)
    {
        switch(sceneName)
        {
            case "Loading":
                sceneType = SceneType.Loading;
                SetGameState(GameState.Waiting);
                break;
            case "Gameplay":
                sceneType = SceneType.Gameplay;
                SetGameState(GameState.Gameplay);
                levelManager = new LevelManager();
                break;
            default:
                break;
        }
    }

    void UpdateGameState()
    {
        switch(gameState)
        {
            case GameState.Waiting:
                break;
            case GameState.Gameplay:
                //Input Update
                inputController.Update();

                //Level Update
                if (levelManager != null)
                {
                    levelManager.Update();
                }

                //Update gameplay stats
                userStats.Update();
                break;
            case GameState.Paused:
                break;
            default:
                break;
        }
    }

    void LateUpdateGameState()
    {
        switch (gameState)
        {
            case GameState.Waiting:
                break;
            case GameState.Gameplay:
                //Level Update
                if (levelManager != null)
                {
                    levelManager.LateUpdate();
                }
                break;
            case GameState.Paused:
                break;
            default:
                break;
        }
    }

    void FixedUpdateGameState()
    {
        switch (gameState)
        {
            case GameState.Waiting:
                break;
            case GameState.Gameplay:
                if (levelManager != null)
                {
                    levelManager.FixedUpdate();
                }
                break;
            case GameState.Paused:
                break;
            default:
                break;
        }
    }

    void SetGameState(GameState newState)
    {
        if (newState == gameState)
        {
            return;
        }
        OnExitGameState(gameState);
        gameState = newState;
        OnEnterGameState(gameState);
    }

    void OnExitGameState(GameState previousState)
    {
        switch (previousState)
        {
            case GameState.Waiting:
                break;
            case GameState.Gameplay:
                break;
            case GameState.Paused:
                break;
            default:
                break;
        }
    }

    void OnEnterGameState(GameState nextState)
    {
        switch (nextState)
        {
            case GameState.Waiting:
                break;
            case GameState.Gameplay:
                break;
            case GameState.Paused:
                break;
            default:
                break;
        }
    }
}
