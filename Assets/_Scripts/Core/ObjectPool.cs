﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolable
{
    void Initialize();

    void OnGet();

    void ResetObject();
}

public class ObjectPool<T> where T: MonoBehaviour, IPoolable, new()
{
    private Queue<T> pool;
    private GameObject objectPrefab;
    private Transform poolParent;

    public ObjectPool(GameObject prefab, Transform parent,string poolName, int poolSize = 10)
    {
        poolParent = GameObject.Instantiate<Transform>(Resources.Load<Transform>("Gameplay/Prefabs/EmptyGameObject"), parent);
        poolParent.name = poolName;
        objectPrefab = prefab;
        pool = new Queue<T>(poolSize);
        for(int i = poolSize; i > 0; --i)
        {
            T obj = GameObject.Instantiate(prefab, poolParent).GetComponent<T>();
            obj.gameObject.SetActive(false);
            obj.Initialize();
            pool.Enqueue(obj);
        }
    }

    public T Get()
    {
        if(pool.Count > 0)
        {
            T obj = pool.Dequeue();
            obj.transform.parent = null;
            obj.OnGet();
            return obj;
        }
        else
        {
            T obj = GameObject.Instantiate(objectPrefab).GetComponent<T>();
            obj.gameObject.SetActive(false);
            obj.Initialize();
            obj.OnGet();
            return obj;
        }
    }

    public void Return(T returnedObj)
    {
        returnedObj.ResetObject();
        returnedObj.transform.SetParent(poolParent);
        pool.Enqueue(returnedObj);
    }
}
