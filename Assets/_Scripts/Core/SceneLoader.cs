﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneType
{
    Launch,
    Loading,
    Main,
    Gameplay
}

public class SceneLoader
{
    public bool isLoading;

    AsyncOperation sceneLoadOp;
    float loadingProgress;

    public IEnumerator LoadScene(string sceneName)
    {
        isLoading = true;
        sceneLoadOp = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        sceneLoadOp.allowSceneActivation = false;
        loadingProgress = 0;
        while(loadingProgress < 1)
        {
            loadingProgress = Mathf.Clamp01(sceneLoadOp.progress / 0.9f);
            yield return null;
        }
        Debug.Log("Scene loaded, waiting for activation");
        yield return new WaitForSeconds(3);
        ActivateLoadedScene();
        GameManager.Instance.OnLoadingFinished(sceneName);
        isLoading = false;
    }

    public void ActivateLoadedScene()
    {
        if(loadingProgress == 1 && sceneLoadOp != null)
        {
            sceneLoadOp.allowSceneActivation = true;
            sceneLoadOp = null;
            SceneManager.UnloadSceneAsync("Loading");
        }        
    }
}
