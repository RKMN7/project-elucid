﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserPrefs
{
    #region Audio Preferences

    public bool masterMute = false;
    public bool musicMute = false;
    public bool sfxMute = false;
    public float masterVolume = 1;
    public float musicVolume = 1;
    public float sfxVolume = 1;

    #endregion

    public UserPrefs()
    {
        SetDefaulValues();
    }

    private void SetDefaulValues()
    {
        this.masterMute = false;
        this.musicMute = false;
        this.sfxMute = false;
        this.masterVolume = 1;
        this.musicVolume = 1;
        this.sfxVolume = 1;
    }

    #region Public Methods
    public void SaveUserPref()
    {
        FileManager.WriteToFile(Application.persistentDataPath + Constants.UserPrefsPath, this);
    }

    public void LoadUserPrefs()
    {
        UserPrefs savedPrefs = FileManager.ReadFromFile<UserPrefs>(Application.persistentDataPath + Constants.UserPrefsPath, true);
        this.CopyFrom(savedPrefs);
    }

    public void ResetPrefs()
    {
        SetDefaulValues();
        SaveUserPref();
    }
    #endregion 

    void CopyFrom(UserPrefs prefs)
    {
        this.masterMute = prefs.masterMute;
        this.musicMute = prefs.musicMute;
        this.sfxMute = prefs.sfxMute;
        this.masterVolume = prefs.masterVolume;
        this.musicVolume = prefs.musicVolume;
        this.sfxVolume = prefs.sfxVolume;
    }
}
