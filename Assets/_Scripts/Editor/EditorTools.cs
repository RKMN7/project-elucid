﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class EditorTools
{
    [MenuItem("Tools/Play from launch")]
    private static void PlayFromLaunchScene()
    {
        EditorSceneManager.OpenScene("Assets/_Scenes/Launch.unity", OpenSceneMode.Single);
        EditorApplication.isPlaying = true;
    }
}
