﻿using System.IO;
using UnityEngine;

public static class FileManager
{
    public static async void WriteToFile(string path, object toWrite)
    {
        string writeText = JsonUtility.ToJson(toWrite);
        using (FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write))
        {
            StreamWriter writer = new StreamWriter(fileStream);
            await writer.WriteAsync(writeText);
            writer.Close();
            Debug.Log(string.Format("Write Complete: {0}", path));
        }
    }

    public static T ReadFromFile<T>(string path, bool writeIfNotExistOrCorrupter = false) where T: new()
    {
        T result;
        if (File.Exists(path))
        {
            string jsonText = File.ReadAllText(path);
            
            result = JsonUtility.FromJson<T>(jsonText);
            if(result == null)
            {
                Debug.Log(string.Format("File at path: '{0}' is corrupted, now deleting and returning default.", path));
                File.Delete(path);
                result = new T();
                if (writeIfNotExistOrCorrupter)
                {
                    WriteToFile(path, result);
                }
            }
            return result;
        }
        else
        {
            Debug.Log(string.Format("File at path: '{0}' does not exist. Returning default", path));
            result = new T();
            if (writeIfNotExistOrCorrupter)
            {
                WriteToFile(path, result);
            }
            return result;
        }
    }
}
