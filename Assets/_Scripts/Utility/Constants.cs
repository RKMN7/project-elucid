﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public const string UserPrefsPath = "/UserPrefs.json";
    public const string UserStatsPath = "/UserStats.json";
}

public static class LayerMasks
{
    public static LayerMask DefaultMask;
    public static LayerMask CharacterMask;
    public static LayerMask HitboxMask;
    public static LayerMask WeaponMask;
    public static LayerMask BulletMask;

    public static void Initialize()
    {
        DefaultMask = LayerMask.GetMask("Default");
        CharacterMask = LayerMask.GetMask("Character");
        HitboxMask = LayerMask.GetMask("HitBox");
        WeaponMask = LayerMask.GetMask("Weapon");
        BulletMask = LayerMask.GetMask("Bullet");
    }
}
