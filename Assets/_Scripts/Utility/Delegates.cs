﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Delegates
{
    namespace Input
    {
        public delegate void TapAction(Vector2 position, int tapCount);
        public delegate void TouchStartAction(Vector2 position, int fingerId);
        public delegate void TouchingAction(Vector2 position, Vector2 deltaPosition, Vector2 direction, float touchTime, int fingerId);
        public delegate void TouchEndAction(Vector2 position, Vector2 deltaPosition, Vector2 direction, float touchTime, int fingerId);
    }

	namespace Gamestate
	{
		public delegate void PlayerDeath();
	}

    namespace Callback
    {
        public delegate void Callback();
    }
}
