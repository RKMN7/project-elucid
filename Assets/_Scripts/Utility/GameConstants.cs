﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConstants
{
    public static float TAP_TIME = 0.18f;
    public static float TAP_DELTA_POS = 10; //In pixels respective to 1080 screen height.
    public static float FIXED_DELTA_TIME = 0.2f;
}
